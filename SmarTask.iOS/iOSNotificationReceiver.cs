﻿using System;

using SmarTask.Interfaces;

using UserNotifications;

using Xamarin.Forms;

namespace SmarTask.iOS
{
    public class iOSNotificationReceiver : UNUserNotificationCenterDelegate
    {
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            DependencyService.Get<INotificationManager>().ReceiveNotification(notification.Request.Content.Title, notification.Request.Content.Body);
            completionHandler(UNNotificationPresentationOptions.Alert);
        }
    }
}