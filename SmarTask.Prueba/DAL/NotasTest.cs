﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SmarTask.COMMON.Entidades;
using SmarTask.COMMON.Validadores;
using SmarTask.DAL.LiteDB;

namespace SmarTask.Prueba
{
    [TestClass]
    public class NotasTest : GenericTest<Nota>
    {
        public NotasTest() : base(new GenericRepository<Nota>(new NotaValidator()),
            new Nota
            {
                Descripcion = "Prueba",
                Nombre = "Prueba",
                UrlImagen = "prueba.jpg"
            },
            new Nota
            {
                Descripcion = "Prueba 2",
                Nombre = "Prueba",
                UrlImagen = "prueba.jpg"
            })
        {
        }
    }
}