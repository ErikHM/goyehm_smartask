﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SmarTask.COMMON.Validadores;
using SmarTask.DAL.LiteDB;
using SmarTask.Entidades;

using System;

namespace SmarTask.Prueba
{
    [TestClass]
    public class ActividadTest : GenericTest<Actividad>
    {
        public ActividadTest() : base(new GenericRepository<Actividad>(new ActividadValidator()), new Actividad
        {
            Categoria = "Prueba",
            Descripcion = "Prueba",
            Fecha = DateTime.Now,
            Nombre = "Prueba",
            Prioridad = true,
            Recordatorio = "Prueba",
            UrlImagen = "prueba.jpg"

        },
            new Actividad
            {
                Categoria = "Prueba",
                Descripcion = "Prueba 2",
                Fecha = DateTime.Now,
                Nombre = "Prueba",
                Prioridad = true,
                Recordatorio = "Prueba",
                UrlImagen = "prueba.jpg"
            })
        {
        }
    }
}