using LiteDB;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;

using System.Linq;

namespace SmarTask.Prueba
{
    [TestClass]
    public abstract class GenericTest<T> where T : BaseDTO
    {
        IGenericRepository<T> _repository;
        T _entidad;
        T _entidadModificada;

        public GenericTest(IGenericRepository<T> repository, T entidad, T entidadModificada)
        {
            _repository = repository;
            _entidad = entidad;
            _entidadModificada = entidadModificada;
        }

        [TestMethod]
        public void Test()
        {
            int numInicial = _repository.Read.Count();
            T insertada = _repository.Create(_entidad);
            Assert.IsNotNull(insertada, "No se inserto");
            Assert.AreEqual(numInicial + 1, _repository.Read.Count(), "El conteo de elementos no es mayor después de insertar un elemento");
            _entidadModificada.Id = insertada.Id;
            T modificada = _repository.Update(_entidadModificada);
            Assert.IsNotNull(modificada, "No se modifico");
            Assert.AreEqual(_entidadModificada, modificada, $"El objeto {typeof(T).Name} no fue modificado ");
            bool r = _repository.Delete(_entidadModificada.Id);
            Assert.IsTrue(r, "No se elimino");
            Assert.AreEqual(numInicial, _repository.Read.Count(), "Después de eliminar, el numero no es igual al inicial");
        }
    }
}
