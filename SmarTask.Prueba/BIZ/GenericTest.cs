﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;

using System.Linq;

namespace SmarTask.Prueba.BIZ
{
    public abstract class GenericTest<T> where T : BaseDTO
    {
        IGenericManager<T> _repository;
        T _entidad;
        T _entidadModificada;

        public GenericTest(IGenericManager<T> repository, T entidad, T entidadModificada)
        {
            _repository = repository;
            _entidad = entidad;
            _entidadModificada = entidadModificada;
        }

        [TestMethod]
        public void Test()
        {
            int numInicial = _repository.ObtenerTodos.Count();
            T insertada = _repository.Insertar(_entidad);
            Assert.IsNotNull(insertada, _repository.Error);
            Assert.AreEqual(numInicial + 1, _repository.ObtenerTodos.Count(), "El conteo de elementos no es mayor después de insertar un elemento");
            _entidadModificada.Id = insertada.Id;
            T modificada = _repository.Modificar(_entidadModificada);
            Assert.IsNotNull(modificada, _repository.Error);
            Assert.AreEqual(_entidadModificada, modificada, $"El objeto {typeof(T).Name} no fue modificado ");
            bool r = _repository.Eliminar(_entidadModificada.Id);
            Assert.IsTrue(r, _repository.Error);
            Assert.AreEqual(numInicial, _repository.ObtenerTodos.Count(), "Después de eliminar, el numero no es igual al inicial");
        }
    }
}
