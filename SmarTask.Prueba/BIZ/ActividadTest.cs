﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SmarTask.BIZ;
using SmarTask.COMMON.Validadores;
using SmarTask.DAL.LiteDB;
using SmarTask.Entidades;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmarTask.Prueba.BIZ
{
    [TestClass]
    public class ActividadTest : GenericTest<Actividad>
    {
        ActividadManager actividadManager = new ActividadManager(new GenericRepository<Actividad>(new ActividadValidator()));
        public ActividadTest() : base(new ActividadManager(new GenericRepository<Actividad>(new ActividadValidator())), new Actividad
        {
            Categoria = "Prueba",
            Descripcion = "Prueba",
            Fecha = DateTime.Now,
            Nombre = "Prueba",
            Prioridad = true,
            Recordatorio = "Prueba",
            UrlImagen = "prueba.jpg"
        },
            new Actividad
            {
                Categoria = "Prueba",
                Descripcion = "Prueba 2",
                Fecha = DateTime.Now,
                Nombre = "Prueba",
                Prioridad = true,
                Recordatorio = "Prueba",
                UrlImagen = "prueba.jpg"
            })
        {
        }

        [TestMethod]
        public void MostrarActividadesParaHoyTest()
        {
            Actividad actividad = actividadManager.Insertar(new Actividad
            {
                Categoria = "Prueba",
                Descripcion = "Prueba",
                Fecha = DateTime.Now,
                Nombre = "Prueba",
                Prioridad = true,
                Recordatorio = "Prueba",
                UrlImagen = "prueba.jpg"
            });

            Assert.IsNotNull(actividad, "No se inserto la actividad");
            List<Actividad> actividades = actividadManager.MostrarActividadesParaHoy(DateTime.Now).ToList();
            Assert.AreEqual(actividades.Count, 1, "No hay la cantidad de registros necesarios");
            Assert.IsTrue(actividadManager.Eliminar(actividad.Id), "No se elimino la entidad");
        }

        [TestMethod]
        public void MostrarActividadesPrioridadTest()
        {
            Actividad actividad = actividadManager.Insertar(new Actividad
            {
                Categoria = "Prueba",
                Descripcion = "Prueba",
                Fecha = DateTime.Now,
                Nombre = "Prueba",
                Prioridad = true,
                Recordatorio = "Prueba",
                UrlImagen = "prueba.jpg"
            });

            Assert.IsNotNull(actividad, "No se inserto la actividad");
            List<Actividad> actividades = actividadManager.MostrarActividadesPrioridad(true).ToList();
            Assert.AreEqual(actividades.Count, 1, "No hay la cantidad de registros necesarios");
            Assert.IsTrue(actividadManager.Eliminar(actividad.Id), "No se elimino la entidad");
        }
    }
}