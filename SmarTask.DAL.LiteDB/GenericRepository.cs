﻿using FluentValidation;
using FluentValidation.Results;

using LiteDB;

using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace SmarTask.DAL.LiteDB
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        string dbName;
        string direccion;
        string tableName;
        private LiteDatabase db;
        private AbstractValidator<T> _validador;

        public GenericRepository(AbstractValidator<T> validador)
        {
            _validador = validador;
            direccion = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            VerificarSiLaDireccionExiste(direccion);
            dbName = $@"{direccion}\SmarTask.db";
            tableName = typeof(T).Name;
            db = new LiteDatabase(dbName);
        }

        private void VerificarSiLaDireccionExiste(string direccion)
        {
            if (!Directory.Exists(direccion))
            {
                Directory.CreateDirectory(direccion);
            }
        }

        private LiteCollection<T> Collection() => db.GetCollection<T>(tableName);

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    return Collection().FindAll();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public string Error { get; private set; }

        public T Create(T entidad)
        {
            try
            {
                ValidationResult validationResult = _validador.Validate(entidad);
                if (validationResult.IsValid)
                {
                    entidad.Id = Guid.NewGuid().ToString();
                    Collection().Insert(entidad);
                    return entidad;
                }
                else
                {
                    this.Error = "";
                    foreach (ValidationFailure error in validationResult.Errors)
                    {
                        this.Error += error.ErrorMessage;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                this.Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                return Collection().Delete(e => e.Id == id) > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => this.Read.Where(predicado.Compile());

        public T SearchById(string id) => Query(e => e.Id == id).FirstOrDefault();

        public T Update(T entidad)
        {
            try
            {
                ValidationResult validationResult = _validador.Validate(entidad);
                if (validationResult.IsValid)
                {
                    Collection().Update(entidad);
                    return entidad;
                }
                else
                {
                    this.Error = "";
                    foreach (ValidationFailure error in validationResult.Errors)
                    {
                        this.Error += error.ErrorMessage;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                this.Error = ex.Message;
                return null;
            }
        }
    }
}
