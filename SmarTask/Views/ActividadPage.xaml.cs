﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using SmarTask.BIZ;
using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActividadPage : ContentPage
    {
        Repositorio<Actividad> repositorio;
        Actividad temp;


        public ActividadPage()
        {
            InitializeComponent();

            repositorio = new Repositorio<Actividad>();

            #region ElegirFotoGaleria
            pickPhoto.Clicked += async (sender, args) =>
                {
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        _ = DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                        return;
                    }
                    var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        PhotoSize = PhotoSize.Medium,

                    });


                    if (file == null)
                        return;

                    imgActividad.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        file.Dispose();
                        return stream;
                    });
                }; 
            #endregion
            
        }
        
        private bool ValidaCampos()
        {
            if (string.IsNullOrWhiteSpace(entNombre.Text))
            {
                DisplayAlert("SmarTask", "El campo nombre viene vacío", "OK");
                return false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(entDescripcion.Text))
                {
                    DisplayAlert("SmarTask", "El campo descripción viene vacío", "OK");
                    return false;
                }
                //else
                //{
                //    DisplayAlert("SmarTask", "Existen campos vacios", "OK");
                //    return false;
                //}
            }
            return true;
        }


        private void btnGuardarActividad_Clicked(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                if (repositorio.Create(new Actividad { Nombre = entNombre.Text, Descripcion = entDescripcion.Text, Categoria = lisCategoria.ToString(), }) is null)
                {
                    DisplayAlert("SmarTask", "No se pudo agregar la actividad", "OK");
                }
                else
                {
                    DisplayAlert("SmarTask", "Actividad agregada", "OK");
                    //ActualizarLista();
                }
            }

            Navigation.PushAsync(new MainPage());
        }

        private void ActualizarLista()
        {
            throw new NotImplementedException();
        }
    }
}