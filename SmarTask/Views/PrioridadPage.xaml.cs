﻿using SmarTask.BIZ;
using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;
using SmarTask.Interfaces;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrioridadPage : ContentPage
    {
        FactoryManager _factoryManager;
        IActividadManager _actividadManager;

        public PrioridadPage(FactoryManager factoryManager)
        {
            InitializeComponent();
            _factoryManager = factoryManager;
            _actividadManager = factoryManager.CrearActividadManager;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstPrioridad.ItemsSource = _actividadManager.MostrarActividadesPrioridad();
        }

        private void btnEliminarPrioridad_Clicked(object sender, EventArgs e)
        {
            if (lstPrioridad.SelectedItem is Actividad actividadSeleccionada)
            {
                if (_actividadManager.Eliminar(actividadSeleccionada.Id))
                {
                    DisplayAlert("SmarTask", "Se elimino correctamente la actividad", "OK");
                    lstPrioridad.ItemsSource = _actividadManager.MostrarActividadesPrioridad();
                }
                else
                {
                    DisplayAlert("SmarTask", _actividadManager.Error, "OK");
                }
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a eliminar", "OK");
            }
        }

        private void btnEditarPrioridad_Clicked(object sender, EventArgs e)
        {
            if (lstPrioridad.SelectedItem is Actividad actividadSeleccionada)
            {
               // Tiene que dirigirse a la ventana de actividades
                this.Navigation.PushAsync(new MiDiaAgregarPage(_factoryManager, actividadSeleccionada));
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a modificar", "OK");
            }
        }

        private void lstPrioridad_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
    }
}