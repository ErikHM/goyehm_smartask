﻿using SmarTask.BIZ;
using SmarTask.COMMON.Entidades;
using SmarTask.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotasPage : ContentPage
    {
        FactoryManager _factoryManager;
        INotaManager _notaManager;

        public NotasPage(FactoryManager factoryManager)
        {
            InitializeComponent();
            _factoryManager = factoryManager;
            _notaManager = factoryManager.CrearNotaManager;
        }

        private void lstNotas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstNotas.ItemsSource = _notaManager.ObtenerTodos;
        }

        private void btnNotaAgregar_Clicked(object sender, EventArgs e)
        {
            //this.Navigation.PushAsync(new NotaAgregarPage());
            this.Navigation.PushAsync(new NotaAgregarPage(_factoryManager, new Nota()));

        }

        private void btnEliminarNota_Clicked(object sender, EventArgs e)
        {
            if (lstNotas.SelectedItem is Nota notaSeleccionada)
            {
                if (_notaManager.Eliminar(notaSeleccionada.Id))
                {
                    DisplayAlert("SmarTask", "Se elimino correctamente la nota", "OK");
                }
                else
                {
                    DisplayAlert("SmarTask", _notaManager.Error, "OK");
                }
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una nota a eliminar", "OK");
            }
        }

        private void btnEditarNota_Clicked(object sender, EventArgs e)
        {
            if (lstNotas.SelectedItem is Nota notaSeleccionada)
            {
                this.Navigation.PushAsync(new NotaAgregarPage(_factoryManager, notaSeleccionada));
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una nota a modificar", "OK");
            }
        }
    }
}