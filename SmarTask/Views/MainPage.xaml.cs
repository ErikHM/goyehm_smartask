﻿using SmarTask.BIZ;
using SmarTask.Entidades;
using SmarTask.Interfaces;
using SmarTask.Models;
using SmarTask.Views;
//using SmarTask.Entidades;
using System;
using System.ComponentModel;
using System.Linq;
using System.Timers;

using Xamarin.Forms;

namespace SmarTask
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private FactoryManager _factoryManager;
        INotificationManager notificationManager;
        Timer aTimer;

        public MainPage()
        {
            InitializeComponent();
            Tools.AgregarTimer += MiDiaAgregarPage_AgregarTimer;
            _factoryManager = new FactoryManager();
            notificationManager = DependencyService.Get<INotificationManager>();
            notificationManager.NotificationReceived += (sender, eventArgs) =>
            {
                var evtData = (NotificationEventArgs)eventArgs;
                ShowNotification(evtData.Title, evtData.Message);
            };
            foreach (Actividad item in _factoryManager.CrearActividadManager.MostrarActividadesPrioridad().Where(actividad => actividad.Fecha >= DateTime.Now))
            {
                if (Tools.DefinirFechaHoraNotificacion(item) != 0)
                {
                    Tools.CrearTemporizador(item);
                }
            }
        }
        void ShowNotification(string title, string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var msg = new Label()
                {
                    Text = $"Notification Received:\nTitle: {title}\nMessage: {message}"
                };
                DisplayAlert("SmarTask", $"{title} \t{message}", "OK");
            });
        }

        private void btnDia_Clicked(object sender, EventArgs e)
        {
            MiDiaPage miDiaPage = new MiDiaPage(_factoryManager);
            miDiaPage.AgregarTimer += MiDiaAgregarPage_AgregarTimer;
            this.Navigation.PushAsync(miDiaPage);
        }

        private void MiDiaAgregarPage_AgregarTimer(object sender, object[] e)
        {
            aTimer = new Timer(double.Parse(e[0].ToString()));
            aTimer.Elapsed += (object s, ElapsedEventArgs t) =>
            {
                string title = e[1].ToString();
                string message = e[2].ToString();
                notificationManager.ScheduleNotification(title, message);
            };
            aTimer.AutoReset = false;
            aTimer.Enabled = true;
            Tools.Hilos.Add(new ActividadHilo { Hilo = aTimer, IdActividad = e[3].ToString() });
        }

        private void btnActividades_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new MiDiaAgregarPage(_factoryManager, new Actividad()));
            //this.Navigation.PushAsync(new MiDiaPage(_factoryManager));
            //this.Navigation.PushAsync(new ActividadPage());
        }

        private void btnPlaneado_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new PlaneadoPage(_factoryManager));
        }

        private void btnTiempo_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new MiTiempoPage());
        }

        private void btnPrioridad_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new PrioridadPage(_factoryManager));
        }

        private void btnNotas_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new NotasPage(_factoryManager));
        }
    }
}