﻿using SmarTask.BIZ;
using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;
using SmarTask.Interfaces;

using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaneadoPage : ContentPage
    {
        FactoryManager _factoryManager;
        IActividadManager _actividadManager;
        INotificationManager _notificationManager;

        public PlaneadoPage(FactoryManager factoryManager)
        {
            InitializeComponent();
            _factoryManager = factoryManager;
            _actividadManager = factoryManager.CrearActividadManager;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstPlaneado.ItemsSource = _actividadManager.ObtenerTodos.Where(actividad => !string.IsNullOrWhiteSpace(actividad.Recordatorio));
        }
        private void lstPlaneado_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }

        private void btnEliminarPlaneado_Clicked(object sender, EventArgs e)
        {
            if (lstPlaneado.SelectedItem is Actividad actividadSeleccionada)
            {
                if (_actividadManager.Eliminar(actividadSeleccionada.Id))
                {
                    DisplayAlert("SmarTask", "Se elimino correctamente la actividad", "OK");
                    lstPlaneado.ItemsSource = _actividadManager.ObtenerTodos;
                }
                else
                {
                    DisplayAlert("SmarTask", _actividadManager.Error, "OK");
                }
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a eliminar", "OK");
            }
        }

        private void btnEditarPlaneado_Clicked(object sender, EventArgs e)
        {
            if (lstPlaneado.SelectedItem is Actividad actividadSeleccionada)
            {
                this.Navigation.PushAsync(new MiDiaAgregarPage(_factoryManager, actividadSeleccionada));
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a modificar", "OK");
            }
        }
    }
}