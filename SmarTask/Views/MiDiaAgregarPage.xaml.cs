﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using SmarTask.BIZ;
using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;
using SmarTask.Interfaces;

using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MiDiaAgregarPage : ContentPage
    {
        Actividad _actividadAuxiliar;
        IActividadManager _actividadManager;
        Actividad _actividad;

        public MiDiaAgregarPage(FactoryManager factoryManager, Actividad actividad)
        {
            InitializeComponent();
            _actividadAuxiliar =new Actividad { Fecha = actividad.Fecha, Recordatorio = actividad.Recordatorio };
            _actividadManager = factoryManager.CrearActividadManager;
            dtpDia.Date = DateTime.Now;
            tmpHora.Time = new TimeSpan(actividad.Fecha.Hour, actividad.Fecha.Minute, actividad.Fecha.Second);
            _actividad = actividad;
            imgActividad.Source = ImageSource.FromFile(actividad.UrlImagen);
            this.BindingContext = _actividad;

            chkRecordar.IsChecked = !string.IsNullOrWhiteSpace(actividad.Recordatorio);

            pickPhoto.Clicked += async (sender, args) =>
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("SmarTask", "No se ha otorgado los suficientes privilegios para poder acceder a su galería", "OK");
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,
                });


                if (file == null)
                    return;

                actividad.UrlImagen = file.Path;

                imgActividad.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            };
        }

        private void btnGuardarMiDia_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (chkRecordar.IsChecked && pckTiempoPrevio.SelectedItem == null)
                {
                    throw new Exception("Debe seleccionar el tiempo previo con el que desea obtener un recordatorio");
                }
                if (string.IsNullOrEmpty(_actividad.Categoria) || string.IsNullOrEmpty(_actividad.Descripcion) || string.IsNullOrEmpty(_actividad.Nombre))
                {
                    throw new Exception("Faltan datos por llenar");
                }
                if (DateTime.Now > new DateTime(dtpDia.Date.Year, dtpDia.Date.Month, dtpDia.Date.Day, tmpHora.Time.Hours, tmpHora.Time.Minutes, tmpHora.Time.Seconds))
                {
                    throw new Exception("La fecha de la actividad debe ser posterior al día de hoy");
                }
                _actividad.Fecha = new DateTime(dtpDia.Date.Year, dtpDia.Date.Month, dtpDia.Date.Day, tmpHora.Time.Hours, tmpHora.Time.Minutes, tmpHora.Time.Seconds);
                if (!string.IsNullOrWhiteSpace(_actividad.Recordatorio) && Tools.DefinirFechaHoraNotificacion(_actividad) == 0)
                {
                    throw new Exception("No se puede hacer el recordatorio en ese intervalo de tiempo");
                }

                _actividad.Recordatorio = chkRecordar.IsChecked ? _actividad.Recordatorio : "";

                if (string.IsNullOrWhiteSpace(_actividad.Id))
                {
                    if (_actividadManager.Insertar(_actividad) is Actividad actividadInsertada)
                    {
                        DisplayAlert("SmarTask", "Actividad agregada exitosamente", "OK");
                        if (!string.IsNullOrWhiteSpace(_actividad.Recordatorio))
                        {
                            Tools.CrearTemporizador(actividadInsertada);
                            //AgregarTimer(this, new object[] { Tools.DefinirFechaHoraNotificacion(_actividad), actividadInsertada.Nombre, actividadInsertada.Descripcion, actividadInsertada.Id });
                        }
                        this.Navigation.PopAsync();
                    }
                    else
                    {
                        throw new Exception(_actividadManager.Error);
                    }
                }
                else
                {
                    if (_actividadManager.Modificar(_actividad) is Actividad actividadInsertada)
                    {
                        DisplayAlert("SmarTask", "Actividad modificada exitosamente", "OK");
                        if (!string.IsNullOrWhiteSpace(_actividad.Recordatorio))
                        {
                            Tools.RemplazarTemporizador(actividadInsertada, Tools.DefinirFechaHoraNotificacion(_actividadAuxiliar) > 0);
                        }
                        this.Navigation.PopAsync();
                    }
                    else
                    {
                        throw new Exception(_actividadManager.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("SmarTask", ex.Message, "OK");
            }
        }
    }
}