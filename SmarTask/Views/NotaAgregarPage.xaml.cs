﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using SmarTask.BIZ;
using SmarTask.COMMON.Entidades;
using SmarTask.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotaAgregarPage : ContentPage
    {
        INotaManager _notaManager;
        Nota _nota;

        public NotaAgregarPage(FactoryManager factoryManager, Nota nota)
        {
            InitializeComponent();
            _notaManager = factoryManager.CrearNotaManager;
            _nota = nota;
            imgNota.Source = ImageSource.FromFile(nota.UrlImagen);
            this.BindingContext = _nota;

            pickPhoto.Clicked += async (sender, args) =>
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("SmarTask", "No se ha otorgado los suficientes privilegios para poder acceder a su galería", "OK");
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,

                });


                if (file == null)
                    return;

                nota.UrlImagen = file.Path;

                imgNota.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            };
        }

        private void btnGuardarNota_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_nota.Id))
            {
                if (_notaManager.Insertar(_nota) is Nota notaInsertada)
                {
                    DisplayAlert("SmarTask", "Nota agregada exitosamente", "OK");
                    this.Navigation.PopAsync();
                }
                else
                {
                    throw new Exception(_notaManager.Error);
                }
            }
            else
            {
                if (_notaManager.Modificar(_nota) is Nota notaInsertada)
                {
                    DisplayAlert("SmarTask", "Nota modificada exitosamente", "OK");
                    this.Navigation.PopAsync();
                }
                else
                {
                    throw new Exception(_notaManager.Error);
                }
            }
        }
    }
}