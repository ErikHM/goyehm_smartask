﻿using System.Collections.Generic;
using System.Linq;

using Microcharts;
using Entry = Microcharts.Entry;
using SkiaSharp;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SmarTask.Entidades;

namespace SmarTask.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MiTiempoPage : ContentPage
    {
        List<KeyValuePair<string, SKColor>> categorias = new List<KeyValuePair<string, SKColor>> { new KeyValuePair<string, SKColor>("Entretenimiento", SKColor.Parse("#46BBA0")), new KeyValuePair<string, SKColor>("Educación", SKColor.Parse("#E4886F")), new KeyValuePair<string, SKColor>("Deporte", SKColor.Parse("#9585C9")), new KeyValuePair<string, SKColor>("Otra", SKColor.Parse("#E56490")) };
        Repositorio<Actividad> repositorioActividad = new Repositorio<Actividad>();
        List<Entry> entries;
        float porcentaje;

        public MiTiempoPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarGrafica();
        }

        private void LlenarGrafica()
        {
            entries = new List<Entry>();
            foreach (KeyValuePair<string,SKColor> categoria in categorias)
            {
                porcentaje = repositorioActividad.Query(actividad => actividad.Categoria == categoria.Key).Count();
                porcentaje /= repositorioActividad.Read.Count();
                porcentaje *= 100;
                entries.Add(new Entry(repositorioActividad.Query(actividad => actividad.Categoria == categoria.Key).Count())
                {
                    Label = categoria.Key,
                    ValueLabel = $"{porcentaje}%",
                    Color = categoria.Value
                });
            }
            grafica.Chart = new RadialGaugeChart { Entries = entries };
        }
    }
}