﻿using SmarTask.BIZ;
using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;
using SmarTask.Interfaces;

using System;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmarTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MiDiaPage : ContentPage
    {
        FactoryManager _factoryManager;
        IActividadManager _actividadManager;

        public event EventHandler<object[]> AgregarTimer;

        public MiDiaPage(FactoryManager factoryManager)
        {
            InitializeComponent();
            _factoryManager = factoryManager;
            _actividadManager = factoryManager.CrearActividadManager;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstMiDia.ItemsSource = _actividadManager.MostrarActividadesParaHoy(DateTime.Now);
        }

        private void btnAgregarMiDia_Clicked(object sender, EventArgs e)
        {
            MiDiaAgregarPage miDiaAgregarPage = new MiDiaAgregarPage(_factoryManager, new Actividad());
            this.Navigation.PushAsync(miDiaAgregarPage);
        }

        private void MiDiaAgregarPage_AgregarTimer(object sender, object[] e)
        {
            AgregarTimer(this, e);
        }

        private void btnEditarMiDia_Clicked(object sender, EventArgs e)
        {
            if (lstMiDia.SelectedItem is Actividad actividadSeleccionada)
            {
                this.Navigation.PushAsync(new MiDiaAgregarPage(_factoryManager,actividadSeleccionada));
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a modificar", "OK");
            }
        }

        private void btnEliminarMiDia_Clicked(object sender, EventArgs e)
        {
            if (lstMiDia.SelectedItem is Actividad actividadSeleccionada)
            {
                if (_actividadManager.Eliminar(actividadSeleccionada.Id))
                {
                    DisplayAlert("SmarTask", "Se elimino correctamente la actividad", "OK");
                    lstMiDia.ItemsSource = _actividadManager.MostrarActividadesParaHoy(DateTime.Now);
                }
                else
                {
                    DisplayAlert("SmarTask", _actividadManager.Error, "OK");
                }
            }
            else
            {
                DisplayAlert("SmarTask", "Debe seleccionar primero una actividad a eliminar", "OK");
            }
        }

        private void lstMiDia_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
    }
}