﻿using SmarTask.Entidades;
using SmarTask.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Timers;

namespace SmarTask
{
    public static class Tools
    {
        public static List<ActividadHilo> Hilos { get; set; }

        public static event EventHandler<object[]> AgregarTimer;

        public static void CrearTemporizador(Actividad item)
        {
            AgregarTimer(new object(), new object[] { DefinirFechaHoraNotificacion(item), item.Nombre, item.Descripcion, item.Id });
        }

        public static bool EliminarTemporizador(Actividad item,bool sePuedeParar)
        {
            try
            {
                if (sePuedeParar)
                {
                    ActividadHilo actividadHilo = Hilos.FirstOrDefault(actividad => actividad.IdActividad == item.Id);
                    actividadHilo.Hilo.Stop();
                    Hilos.Remove(actividadHilo);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemplazarTemporizador(Actividad item,bool sePuedeParar)
        {
            try
            {
                if (EliminarTemporizador(item,sePuedeParar))
                {
                    CrearTemporizador(item);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static double DefinirFechaHoraNotificacion(Actividad actividad)
        {
            DateTime fecha = actividad.Fecha;
            DateTime fechaNueva;
            TimeSpan timeSpan;
            switch (actividad.Recordatorio)
            {
                case "Cinco minutos antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(0, 5, 0));
                    break;
                case "Diez minutos antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(0, 10, 0));
                    break;
                case "Media hora antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(0, 30, 0));
                    break;

                case "Una hora antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(1, 0, 0));
                    break;

                case "Dos horas antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(2, 0, 0));
                    break;
                case "Un día antes":
                    fechaNueva = fecha.Subtract(new TimeSpan(24, 0, 0));
                    break;
                default:
                    fechaNueva = fecha.Subtract(new TimeSpan(48, 0, 0));
                    break;
            }
            if ((fechaNueva - DateTime.Now).TotalMilliseconds > 0)
            {
                return (fechaNueva - DateTime.Now).TotalMilliseconds;
            }
            else
            {
                return 0;
            }
        }
    }
}