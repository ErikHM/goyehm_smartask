﻿using LiteDB;
using SmarTask.Entidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace SmarTask
{
    public class Repositorio<T> where T : BaseDTO
    {
        string dbName;
        string direccion;
        string tableName;
        private LiteDatabase db;

        public Repositorio()
        {
            direccion = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            VerificarSiLaDireccionExiste(direccion);
            dbName = $@"{direccion}\SmarTask.db";
            tableName = typeof(T).Name;
            db = new LiteDatabase(dbName);
        }

        private void VerificarSiLaDireccionExiste(string direccion)
        {
            if (!Directory.Exists(direccion))
            {
                Directory.CreateDirectory(direccion);
            }
        }


        //private LiteCollection<T> Collection() => db.GetCollection<T>(tableName);
        private LiteCollection<T> Collection() => db.GetCollection<T>(tableName);

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    return Collection().FindAll();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                Collection().Insert(entidad);
                return entidad;
            }
            //catch (Exception ex)
            catch (Exception) 
            {
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                //return Collection().Delete(e => e.Id.ToString() == id) > 0 ? true : false;
                return Collection().Delete(e => e.Id.ToString() == id) > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => Read.Where(predicado.Compile());

        public T SearchById(string id) => Query(e => e.Id.ToString() == id).FirstOrDefault();

        public T Update(T entidad)
        {
            try
            {
                Collection().Update(entidad);
                return entidad;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
