﻿using System.Timers;

namespace SmarTask
{
    public class ActividadHilo
    {
        public string IdActividad { get; set; }
        public Timer Hilo { get; set; }
    }
}
