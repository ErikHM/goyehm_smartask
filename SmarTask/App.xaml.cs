﻿
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmarTask
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Tools.Hilos = new List<ActividadHilo>();
            this.MainPage = new NavigationPage(new MainPage());
            //MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
