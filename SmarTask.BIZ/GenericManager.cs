﻿using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmarTask.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> _repository;
        public GenericManager(IGenericRepository<T> repository)
        {
            _repository = repository;
        }

        public string Error => _repository.Error;

        public IEnumerable<T> ObtenerTodos => _repository.Read;

        public T BuscarPorId(string id) => _repository.SearchById(id);

        public bool Eliminar(string id) => _repository.Delete(id);

        public T Insertar(T entidad) => _repository.Create(entidad);

        public T Modificar(T entidad) => _repository.Update(entidad);

        public IEnumerable<T> Consulta(Expression<Func<T, bool>> predicado) => _repository.Query(predicado);
    }
}
