﻿using SmarTask.COMMON.Entidades;
using SmarTask.COMMON.Interfaces;
using SmarTask.COMMON.Validadores;
using SmarTask.DAL.LiteDB;
using SmarTask.Entidades;

namespace SmarTask.BIZ
{
    public class FactoryManager
    {
        public FactoryManager()
        {
            this.CrearActividadManager = new ActividadManager(new GenericRepository<Actividad>(new ActividadValidator()));
            this.CrearNotaManager = new NotaManager(new GenericRepository<Nota>(new NotaValidator()));
        }

        public IActividadManager CrearActividadManager { get; }
        public INotaManager CrearNotaManager { get; }
    }
}
