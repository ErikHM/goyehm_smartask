﻿using SmarTask.COMMON.Entidades;
using SmarTask.COMMON.Interfaces;

namespace SmarTask.BIZ
{
    public class NotaManager : GenericManager<Nota>, INotaManager
    {
        public NotaManager(IGenericRepository<Nota> repository) : base(repository)
        {
        }
    }
}
