﻿using SmarTask.COMMON.Interfaces;
using SmarTask.Entidades;

using System;
using System.Collections.Generic;

namespace SmarTask.BIZ
{
    public class ActividadManager : GenericManager<Actividad>, IActividadManager
    {
        public ActividadManager(IGenericRepository<Actividad> genericRepository) : base(genericRepository)
        {

        }

        public IEnumerable<Actividad> MostrarActividadesParaHoy(DateTime fecha = default) => Consulta(actividad => actividad.Fecha.ToShortDateString() == fecha.ToShortDateString());

        public IEnumerable<Actividad> MostrarActividadesPrioridad(bool prioridad = true) => Consulta(actividad => actividad.Prioridad == prioridad);
    }
}
