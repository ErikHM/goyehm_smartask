﻿using SmarTask.COMMON.Entidades;

namespace SmarTask.COMMON.Interfaces
{
    public interface INotaManager : IGenericManager<Nota>
    {
        //IEnumerable<Nota> MostrarTodasLasNotas();
    }
}
