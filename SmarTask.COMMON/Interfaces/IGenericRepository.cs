﻿using SmarTask.Entidades;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmarTask.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T : BaseDTO
    {
        string Error { get; }
        T Create(T entidad);
        IEnumerable<T> Read { get; }
        T Update(T entidad);
        bool Delete(string id);
        T SearchById(string id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
    }
}
