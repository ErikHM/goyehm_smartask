﻿using SmarTask.Entidades;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmarTask.COMMON.Interfaces
{
    public interface IGenericManager<T> where T : BaseDTO
    {
        string Error { get; }
        T Insertar(T entidad);
        IEnumerable<T> ObtenerTodos { get; }
        T Modificar(T entidad);
        bool Eliminar(string id);
        T BuscarPorId(string id);
        IEnumerable<T> Consulta(Expression<Func<T, bool>> predicado);
    }
}
