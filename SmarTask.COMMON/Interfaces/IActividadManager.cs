﻿using SmarTask.Entidades;

using System;
using System.Collections.Generic;
using System.Text;

namespace SmarTask.COMMON.Interfaces
{
    public interface IActividadManager : IGenericManager<Actividad>
    {

        IEnumerable<Actividad> MostrarActividadesPrioridad(bool prioridad = true);

        IEnumerable<Actividad> MostrarActividadesParaHoy(DateTime fecha = default);
    }
}
