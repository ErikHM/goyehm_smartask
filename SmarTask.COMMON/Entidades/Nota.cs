﻿using SmarTask.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmarTask.COMMON.Entidades
{
    public class Nota : BaseDTO
    {

        public string UrlImagen { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
