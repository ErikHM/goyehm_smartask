﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmarTask.Entidades
{
    public class Actividad : BaseDTO
    {
        //public string Imagen { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Categoria { get; set; }
        public DateTime Fecha { get; set; }
        public string UrlImagen { get; set; }

        //public DateTime Hora { get; set; }

        //public string Recordar { get; set; }
        public bool Prioridad { get; set; }
        public string Recordatorio { get; set; }

        //public override string ToString()
        //{
        //    return Nombre;
        //}
    }
}
