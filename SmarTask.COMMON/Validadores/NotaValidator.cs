﻿using FluentValidation;
using SmarTask.COMMON.Entidades;

namespace SmarTask.COMMON.Validadores
{
    public class NotaValidator:AbstractValidator<Nota>
    {
        public NotaValidator()
        {
            RuleFor(nota => nota.Descripcion).NotEmpty().NotNull();
            RuleFor(nota => nota.Nombre).NotEmpty().NotNull();
        }
    }
}