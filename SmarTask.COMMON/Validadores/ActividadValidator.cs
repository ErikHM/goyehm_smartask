﻿using FluentValidation;
using SmarTask.Entidades;

namespace SmarTask.COMMON.Validadores
{
    public class ActividadValidator:AbstractValidator<Actividad>
    {
        public ActividadValidator()
        {
            RuleFor(actividad => actividad.Categoria).NotEmpty().NotNull();
            RuleFor(actividad => actividad.Descripcion).NotEmpty().NotNull();
            RuleFor(actividad => actividad.Fecha).NotNull();
            RuleFor(actividad => actividad.Nombre).NotEmpty().NotNull();
            //RuleFor(actividad => actividad.Prioridad).NotEmpty().NotNull();
        }
    }
}